FROM python:3.9.0-slim

ENV APP_VERSION="6.1.6" \
    APP="platformio-core"

LABEL app.name="${APP}" \
      app.version="${APP_VERSION}" 

RUN pip install -U platformio==${APP_VERSION} && \
    python3 -m pip install -U pyocd && \
    mkdir -p /workspace && \
    mkdir -p /.platformio && \
    chmod a+rwx /.platformio && \
    apt update && apt install -y git && apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}/ && apt -y install stlink-tools && python3 -m pip install -U pyocd

USER 1001

WORKDIR /workspace

ENTRYPOINT ["platformio"] 
